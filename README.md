# OOP fianl project - RFID Access Control System
Implementation of RFID Access Control System in java environment.

![Snapshot of main GUI](res/main_snapshot.png)

## Table of contents
- [OOP fianl project - RFID Access Control System](#oop-fianl-project---rfid-access-control-system)
  - [Table of contents](#table-of-contents)
  - [Features](#features)
  - [System architecture](#system-architecture)
  - [Communication with RFID reader](#communication-with-rfid-reader)
    - [Data frame](#data-frame)
  - [Directory hierarchy](#directory-hierarchy)
  - [Dependencies](#dependencies)
    - [Java package](#java-package)
    - [Arduino library](#arduino-library)


## Features
- Realtime display RFID card event
- Bidirectional communication with RFID reader
- Editor for add/delete members in database
- Export/import data to/from JSON file

## System architecture
```
RFID reader = PC = software interface
```

## Communication with RFID reader

Via UART, default baudrate `115200`  

### Data frame
Reader to host
```
<UID in hex format>
AABBEEFF\n
```
Concatenate all HEX value in a string.

Host to reader
```
<display message>,<authorized message>
Alice,Pass\n
Bob,Permission denied
```

## Directory hierarchy
```
oop_rfid_acs_2020
├── bin
├── doc
├── lib
├── res
│   ├── Arduino
│   └── db
├── src
│   ├── main
│   └── rfidacs
└── test
    ├── serial
    └── sqldb
```

## Dependencies

### Java package
- [rxtx(librxtx-java)](https://github.com/rxtx/rxtx)
- [SQLite JDBC Driver](https://github.com/xerial/sqlite-jdbc)
- [Gson](https://github.com/google/gson)

### Arduino library
- [MFRC522](https://github.com/miguelbalboa/rfid)
- [Arduino-LiquidCrystal-I2C-library](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library)
