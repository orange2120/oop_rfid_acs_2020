package main;

import java.awt.*;

/**
 * utils
 * Some utilities
 */
public class utils {

    public static String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public static String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }

    public static void addComponent(Container cp, Component comp, int row, int column, int width, int height
    , int constraints) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx=row;
        gbc.gridy=column;
        gbc.gridwidth=width;
        gbc.gridheight=height;
        gbc.weightx = (row == 0) ? 0.1 : 1.0;
        gbc.weighty = 1.0;
        gbc.fill = constraints;
        gbc.insets = new Insets(5, 3, 5, 3);
        cp.add(comp, gbc);
    }
    
}