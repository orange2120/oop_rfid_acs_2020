package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;

import rfidacs.*;

/**
 * memberDBEditor
 * Editor of member DB, add/remove and export members
 */
public class memberDBEditor extends JFrame {

    JTable memberTable = null;
    memberDB mem = null;

    public memberDBEditor() {

        JFrame frame = new JFrame("Member database editor");
        frame.setSize(640, 480);
        frame.setLocationRelativeTo(null);

        Container cp = frame.getContentPane();

        cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));

        JLabel lblPath = new JLabel("Path:");
        JTextField tfMemberDB = new JTextField(dbManager.memberDB_path);

        DefaultTableModel mModel = new DefaultTableModel(0, memberDB.memberColumn.length);
        mModel.setColumnIdentifiers(memberDB.memberColumn);
        memberTable = new JTable(mModel);
        RowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(mModel);
        memberTable.setRowSorter(sorter);
        JScrollPane scrollMemberTable = new JScrollPane(memberTable);

        JButton btnNew = new JButton("New");
        JButton btnOpen = new JButton("Open");
        JButton btnLoad = new JButton("Load");
        JButton btnSave = new JButton("Save");
        JButton btnExport = new JButton("Export");
        JButton btnImport = new JButton("Import");
        
        JButton btnAdd = new JButton("Add");
        JButton btnDel = new JButton("Delete");

        JFileChooser fcMember = new JFileChooser();

        JPanel filePanel = new JPanel(new GridBagLayout());
        JPanel dbPanel = new JPanel();
        
        cp.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        filePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        
        dbPanel.setLayout(new BoxLayout(dbPanel, BoxLayout.X_AXIS));
        dbPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        utils.addComponent(filePanel, lblPath, 0, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(filePanel, tfMemberDB, 1, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(filePanel, btnOpen, 2, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(filePanel, btnLoad, 0, 1, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(filePanel, btnSave, 1, 1, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(filePanel, btnNew, 2, 1, 1, 1, GridBagConstraints.BOTH);
        
        dbPanel.add(btnImport);
        dbPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        dbPanel.add(btnExport);
        dbPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        dbPanel.add(btnAdd);
        dbPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        dbPanel.add(btnDel);

        cp.add(filePanel);
        cp.add(scrollMemberTable);
        cp.add(dbPanel);
        
        // utils.addComponent(dbPanel, btnImport, 0, 0, 1, 1, GridBagConstraints.BOTH);
        // utils.addComponent(dbPanel, btnExport, 1, 0, 1, 1, GridBagConstraints.BOTH);
        // utils.addComponent(dbPanel, btnAdd, 2, 0, 1, 1, GridBagConstraints.BOTH);
        // utils.addComponent(dbPanel, btnDel, 3, 0, 1, 1, GridBagConstraints.BOTH);

        // table edit
        btnAdd.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                DefaultTableModel model = (DefaultTableModel) memberTable.getModel();
                model.addRow(new Object[]{"", "", "", "", ""});
            } 
        });

        btnDel.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                DefaultTableModel model = (DefaultTableModel) memberTable.getModel();
                try {
                    int selectedRowIdx = memberTable.getSelectedRow();
                    model.removeRow(selectedRowIdx);
                }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            } 
        });

        btnOpen.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                int res = fcMember.showOpenDialog(null);
                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File memberFile = fcMember.getSelectedFile();
                    tfMemberDB.setText(memberFile.getAbsolutePath());
                }
            } 
        });

        btnLoad.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
                File f = new File(tfMemberDB.getText());
                if(f.exists() && !f.isDirectory()) 
                { 
                    loadMemberDB(tfMemberDB.getText());
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "File do not exist!", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } 
        });

        btnImport.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                FileNameExtensionFilter ff = new FileNameExtensionFilter("JSON File (.json)", "json");
                fcMember.setFileFilter(ff);
                int res = fcMember.showOpenDialog(null);

                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File memberFile = fcMember.getSelectedFile();
                    Main.dbmanager.getMemberDB().importDB(memberFile.getAbsolutePath());
                    clearMemberTable();
                    updateMemberTable();
                }
            }
        });

        btnExport.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                FileNameExtensionFilter ff = new FileNameExtensionFilter("JSON File (.json)", "json");
                fcMember.setFileFilter(ff);    
                int res = fcMember.showOpenDialog(null);

                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File memberFile = fcMember.getSelectedFile();
                    Main.dbmanager.getMemberDB().exportDB(memberFile.getAbsolutePath());
                }
            }
        });

        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                if (mem != null) {
                    mem.disconnectDB();
                }

                System.out.println("memberDBEditor Closed");
                e.getWindow().dispose();
            }
        });

        frame.setVisible(true);
    }

    /**
     * Load member DB to editor
     * can be separate from online db/
     * @param path
     */ 
    public void loadMemberDB(final String path) {
        mem = new memberDB(path);
        mem.connectDB();
        updateMemberTable();
    }

    public void updateMemberTable() {
        DefaultTableModel model = (DefaultTableModel) memberTable.getModel();
        ArrayList<Object[]> result = (ArrayList<Object[]>)mem.queryAllMember();
        
        for (Object[] o : result)
        {
            model.addRow(o);
        }
    }

    public void clearMemberTable() {
        DefaultTableModel eModel = new DefaultTableModel(0, memberDB.memberColumn.length);
        eModel.setColumnIdentifiers(memberDB.memberColumn);
        memberTable.setModel(eModel); // remove all contents in event table
    }
}