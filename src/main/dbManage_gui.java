
package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

/**
 * dbManage_gui
 * GUI of DB manage
 */
class dbManage_gui extends JFrame {

    public dbManage_gui() {
        JFrame frame = new JFrame("DB Management");

        frame.setSize(640, 100);
        frame.setLocationRelativeTo(null);

        Container cp = frame.getContentPane();
        cp.setLayout(new GridLayout(2, 3, 10, 5));

        JLabel lblMemberDB = new JLabel("Member DB:");
        JLabel lblEventDB = new JLabel("Event DB:");

        JTextField tfMember = new JTextField(dbManager.memberDB_path);
        JTextField tfEvent = new JTextField(dbManager.eventDB_path);
        tfMember.setHorizontalAlignment(SwingConstants.RIGHT);
        tfEvent.setHorizontalAlignment(SwingConstants.RIGHT);

        JFileChooser fcMember = new JFileChooser();
        JFileChooser fcEvent  = new JFileChooser();

        JButton btnMemberDBPath = new JButton("browse...");
        JButton btnEventDBPath = new JButton("browse...");

        JButton btnMemberDBReload = new JButton("Reload");
        JButton btnEventDBReload = new JButton("Reload");

        JButton btnNewMemberDB = new JButton("New");
        JButton btnNewEventDB = new JButton("New");

        cp.add(lblMemberDB);
        cp.add(tfMember);
        cp.add(btnMemberDBPath);
        cp.add(btnMemberDBReload);

        cp.add(lblEventDB);
        cp.add(tfEvent);
        cp.add(btnEventDBPath);
        cp.add(btnEventDBReload);

        btnMemberDBPath.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                int res = fcMember.showOpenDialog(null);
                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File memberFile = fcMember.getSelectedFile();
                    tfMember.setText(memberFile.getAbsolutePath());
                }

            } 
        });

        btnEventDBPath.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                int res = fcEvent.showOpenDialog(null);
                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File eventFile = fcEvent.getSelectedFile();
                    tfEvent.setText(eventFile.getAbsolutePath());
                }

            } 
        });

        btnEventDBReload.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                Main.dbmanager.getEventDB().disconnectDB();
                Main.dbmanager.eventDB_path = tfEvent.getText();
                Main.dbmanager.getEventDB().connectDB();
            }
        });

        btnMemberDBReload.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                Main.dbmanager.getMemberDB().disconnectDB();
                Main.dbmanager.memberDB_path = tfMember.getText();
                Main.dbmanager.getMemberDB().connectDB();
            }
        });

        frame.setVisible(true);
    }
    
}