package main;

import rfidacs.*;

/**
 * dbManager
 * Manage DB operations
 */
public class dbManager {
    // minimize overlapped information between db
    public static String memberDB_path = "./res/db/member_test.db";
    // public static String memberDB_path = "./res/db/member.db";
    public static String eventDB_path = "./res/db/event_test.db";
    // public static String eventDB_path = "./res/db/event.db";

    public eventDB eDB = null;
    public memberDB mDB = null;

    /**
     * Initialize(connect) DBs
     */
    public void initDB() {
        eDB = new eventDB(eventDB_path);
        mDB = new memberDB(memberDB_path);
        try {
            mDB.connectDB();
            eDB.connectDB();

            System.out.println("DB connected!");
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Disconnect DBs
     */
    public void discDB() {
        try {
            eDB.disconnectDB();
            mDB.disconnectDB();
            
            System.out.println("DB disc.");
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public eventDB getEventDB() {
        return eDB;
    }

    public memberDB getMemberDB() {
        return mDB;
    }

    
}