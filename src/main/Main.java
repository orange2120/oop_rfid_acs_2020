package main;

import javax.swing.SwingUtilities;

/**
 * Main Main function
 */
public class Main {
    public static Main_gui gui = null;
    public static dbManager dbmanager = null;
    public static void main(String[] args) throws Exception {
        dbmanager = new dbManager();
        dbmanager.initDB();

        SwingUtilities.invokeLater(
          new Runnable(){
            public void run(){
                gui = new Main_gui();
        }});

        System.out.println("Exit.");
    }
}