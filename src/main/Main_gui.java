package main;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.File;
import java.util.List;

import rfidacs.*;

/**
 * Main_gui
 * GUI of main
 */
public class Main_gui extends JFrame {
    
    private JTable eventTable = null;
    public static TwoWaySerialComm com = null;
    public final static String appIcon = "./res/icon.png"; // application icon
    
    private JFrame frame = null;

    private final String frameTitle = "RFID Access Control System";
    private final String aboutMsg = "RFID access control application based on JAVA\nWITHOUT ANY WARRANTY!";

    // info panel
    private JLabel lblID = null;
    private JLabel lblName = null;
    private JLabel lblLast = null;
    private JLabel lblImg = null;
    private ImageIcon passIcon = null;
    private ImageIcon deniedIcon = null;
    private ImageIcon errIcon = null;
    
    public Main_gui() {

        com = new TwoWaySerialComm();

        frame = new JFrame(frameTitle);
    
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1024, 768); // set window size
        frame.setLocationRelativeTo(null);

        // set frame icon
        ImageIcon img = new ImageIcon(appIcon);
        frame.setIconImage(img.getImage());
    
        Container cp = frame.getContentPane();
        cp.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        buildMenuBar(menuBar);

        // receiving event table
        DefaultTableModel eModel = new DefaultTableModel(0, eventDB.eventColumn.length);
        eModel.setColumnIdentifiers(eventDB.eventColumn);
        eventTable = new JTable(eModel);
        RowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(eModel);
        eventTable.setRowSorter(sorter);
        JScrollPane scrollEventTable = new JScrollPane(eventTable);

        JPanel topPanel = new JPanel(new GridBagLayout());
        JPanel centerPanel = new JPanel(new GridBagLayout());
        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel infoPanel = new JPanel();

        topPanel.add(Box.createVerticalStrut(10));
        topPanel.setBorder(BorderFactory.createTitledBorder("Reader connection"));
        buidTopPanel(topPanel);
        cp.add(topPanel);
        
        centerPanel.add(Box.createVerticalStrut(10));
        infoPanel.setBorder(BorderFactory.createTitledBorder("Info"));
        cp.add(centerPanel);
        
        bottomPanel.add(Box.createVerticalStrut(10));
        cp.add(bottomPanel);

        // <row>, <column>, <width>, <height>
        // centerPanel
        utils.addComponent(centerPanel, scrollEventTable, 0, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(centerPanel, infoPanel, 1, 0, 1, 1, GridBagConstraints.BOTH);
        
        
        buildInfoPanel(infoPanel);
        // bottomPanel
        buildBottomPanel(bottomPanel);

        frame.setVisible(true);

    //----------------------------------------------------------------------
    //    button events
    //----------------------------------------------------------------------

        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                Main.dbmanager.discDB();
                System.out.println("Main_gui Closed");
                e.getWindow().dispose();
            }
        });

    }

    public void buildMenuBar(JMenuBar menuBar)
    {
        JMenu menuFile = new JMenu("File");
        JMenuItem itQuit = new JMenuItem("Quit");
        menuFile.add(itQuit);
        
        JMenu menuTool = new JMenu("Tools");
        JMenuItem itDBMan = new JMenuItem("DB Manager");
        JMenuItem itMDBEdit = new JMenuItem("Member DB Editor");
        menuTool.add(itDBMan);
        menuTool.add(itMDBEdit);

        JMenu menuHelp = new JMenu("Help");
        JMenuItem itDoc = new JMenuItem("Documentation");
        JMenuItem itAbout = new JMenuItem("About");
        menuHelp.add(itDoc);
        menuHelp.addSeparator();
        menuHelp.add(itAbout);

        menuBar.add(menuFile);
        menuBar.add(menuTool);
        menuBar.add(menuHelp);

        itDBMan.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                JFrame dbmanage = new dbManage_gui();
            } 
        });

        itMDBEdit.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                JFrame memedbeditor = new memberDBEditor();
            } 
        });

        itQuit.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                int result = JOptionPane.showConfirmDialog(null, "Are you sure?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (result==JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            } 
        });

        itAbout.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                JOptionPane.showMessageDialog(null, aboutMsg, "About", JOptionPane.INFORMATION_MESSAGE);
            } 
        });
    }

    public void buidTopPanel(JPanel panel)
    {
        JLabel lblConnStat = new JLabel("Offline"); // connect button
        lblConnStat.setForeground(Color.RED);
        lblConnStat.setFont (lblConnStat.getFont().deriveFont (48.0f));
        JLabel lblComPort = new JLabel("Port:"); // port name
        JLabel lblComBaud = new JLabel("Baud rate:");

        JButton btnRescan = new JButton("Rescan");
        JButton btnComConn = new JButton("Connect");

        JComboBox<String> jcbComPort = new JComboBox<>(com.getPort().toArray(new String[0]));
        JComboBox<Integer> jcbComBaud = new JComboBox<>(TwoWaySerialComm.baudRates);
        jcbComPort.setEditable(true);
        jcbComBaud.setSelectedIndex(4); // select default baudrate

        utils.addComponent(panel, lblComPort, 0, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblComBaud, 0, 1, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, jcbComPort, 1, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, jcbComBaud, 1, 1, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, btnRescan , 2, 0, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, btnComConn, 2, 1, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblConnStat, 3, 0, 5, 2, GridBagConstraints.CENTER);

        // scan port button event
        btnRescan.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                // update available serial ports
                jcbComPort.removeAllItems();
                DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(com.getPort().toArray(new String[0]));
                jcbComPort.setModel(model);
            }
        });

        btnComConn.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                // try to connect to serial port
                if (!com.isConnected()) {
                    try {
                        com.connect((String)jcbComPort.getSelectedItem(), (Integer)jcbComBaud.getSelectedItem());
                        jcbComPort.setEnabled(false);
                        jcbComBaud.setEnabled(false);
                        btnRescan.setEnabled(false);
                        btnComConn.setText("Disconnect");
                        lblConnStat.setText("Online");
                        lblConnStat.setForeground(Color.GREEN);
                    }
                    catch ( Exception ex ) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Port connnection error!\nPlease check and try again.", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else {
                    com.disconnect();
                    jcbComPort.setEnabled(true);
                    jcbComBaud.setEnabled(true);
                    btnRescan.setEnabled(true);
                    btnComConn.setText("Connect");
                    lblConnStat.setText("Offline");
                    lblConnStat.setForeground(Color.RED);
                }
            } 
        });
    }

    public void buildInfoPanel(JPanel panel)
    {
        panel.setLayout(new GridBagLayout());

        BufferedImage passImage = null;
        BufferedImage deniedImage = null;
        BufferedImage errorImage = null;

        try
        {
            passImage = ImageIO.read(new File(memberDB.passImg));
            deniedImage = ImageIO.read(new File(memberDB.deniedImg));
            errorImage = ImageIO.read(new File(memberDB.errorImg));
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }

        passIcon = new ImageIcon(passImage.getScaledInstance(100, 100, Image.SCALE_SMOOTH));
        deniedIcon = new ImageIcon(deniedImage.getScaledInstance(100, 100, Image.SCALE_SMOOTH));
        errIcon = new ImageIcon(errorImage.getScaledInstance(100, 100, Image.SCALE_SMOOTH));

        lblImg = new JLabel();
        lblImg.setIcon(passIcon);

        JLabel lblIDTitle = new JLabel("ID:");
        lblID = new JLabel("----");
        JLabel lblNameTitle = new JLabel("Name:");
        lblName = new JLabel("----");
        JLabel lblLastTitle = new JLabel("Last login:");
        lblLast = new JLabel("----");

        JPanel RInfoPanel = new JPanel(new GridBagLayout());

        // for debugging
        // panel.setBackground(Color.green);
        // RInfoPanel.setBackground(Color.red);

        lblIDTitle.setFont(lblID.getFont().deriveFont(20.0f));
        lblNameTitle.setFont(lblName.getFont().deriveFont(20.0f));
        lblLastTitle.setFont(lblName.getFont().deriveFont(20.0f));

        lblID.setFont(lblID.getFont().deriveFont(36.0f));
        lblName.setFont(lblName.getFont().deriveFont(36.0f));
        lblLast.setFont(lblLast.getFont().deriveFont(36.0f));

        utils.addComponent(panel, lblIDTitle, 0, 1, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblID, 0, 2, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblNameTitle, 0, 3, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblName, 0, 4, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblLastTitle, 0, 5, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, lblLast, 0, 6, 1, 1, GridBagConstraints.BOTH);
        utils.addComponent(panel, RInfoPanel, 1, 0, 1, 7, GridBagConstraints.BOTH);

        RInfoPanel.add(lblImg);
    }

    public void buildBottomPanel(JPanel panel) {

        JButton btnExport = new JButton("Export");
        JButton btnClear = new JButton("Clear");
        JButton btnQuery = new JButton("Query");
        JButton btnLoadHis = new JButton("Load history");

        JFileChooser fcEvent = new JFileChooser();

        panel.add(btnClear);
        panel.add(btnQuery);
        panel.add(btnExport);
        panel.add(btnLoadHis);

        btnQuery.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {

            }

        });

        btnExport.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                FileNameExtensionFilter ff = new FileNameExtensionFilter("JSON File (.json)", "json");
                fcEvent.setFileFilter(ff);    
                int res = fcEvent.showOpenDialog(null);

                if (res == JFileChooser.APPROVE_OPTION)
                {
                    File eventFile = fcEvent.getSelectedFile();
                    Main.dbmanager.getEventDB().exportDB(eventFile.getAbsolutePath());
                }
            } 
        });

        btnLoadHis.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                List<Object[]> obj = Main.dbmanager.getEventDB().loadHistory();
                for (Object[] o: obj)
                {
                    addEventTable(o);
                }
            } 
        });

        btnClear.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) 
            {
                DefaultTableModel eModel = new DefaultTableModel(0, eventDB.eventColumn.length);
                eModel.setColumnIdentifiers(eventDB.eventColumn);
                eventTable.setModel(eModel); // remove all contents in event table
            } 
        });

        // utils.addComponent(bottomPanel, btnExport, 0, 0, 1, 1, GridBagConstraints.BOTH);
        // utils.addComponent(bottomPanel, btnLoadHis, 1, 0, 1, 1, GridBagConstraints.BOTH);
        // utils.addComponent(bottomPanel, btnClear, 2, 0, 1, 1, GridBagConstraints.BOTH);
        
    }

    public void addEventTable(Object[] obj)
    {
        DefaultTableModel tm = (DefaultTableModel)eventTable.getModel();
        tm.addRow(obj);   
    }
    
    /**
     * Set info panel information
     * @param id
     * @param name
     * @param last
     * @param auth
     */
    public void setInfo(String id, String name, String last, int auth)
    {
        if (auth == 1) // auth sucess
        {
            lblImg.setIcon(passIcon);
            lblID.setForeground(Color.BLACK);
            lblID.setText(id);
            lblName.setText(name);
            lblLast.setText(last);
        }
        else if (auth == 2) // access denied
        {
            lblImg.setIcon(deniedIcon);
            lblID.setForeground(Color.BLACK);
            lblID.setText(id);
            lblName.setText(name);
            lblLast.setText(last);
        }
        else // other cases
        {
            lblImg.setIcon(errIcon);
            lblID.setForeground(Color.RED);
            lblID.setText("Not found");
            lblName.setText("-----");
            lblLast.setText("-----");
        }
    }

    public static void main(String[] args) {
        System.out.println("Main_gui");
    }
}