package rfidacs;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import main.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.List;

/**
 * This version of the TwoWaySerialComm example makes use of the 
 * SerialPortEventListener to avoid polling.
 *
 */
public class TwoWaySerialComm
{
    public static final int timeout = 2000; // ms
    public static final Integer[] baudRates = new Integer[] {9600, 19200, 38400, 57600, 115200, 230400, 921600};

    private CommPort commPort = null;

    private OutputStream out = null;
    private static SerialPort serialPort = null;
    private static Thread sendThread;

    public TwoWaySerialComm()
    {
        super();
    }
    
    public List<String> getPort()
    // public List<String> getPort()
    {
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();
        
        List<String> serialList = new ArrayList<>();

        while(ports.hasMoreElements())
        {  
            CommPortIdentifier cpIdentifier = (CommPortIdentifier)ports.nextElement();  
            serialList.add(cpIdentifier.getName());
        }
    
        return serialList;
    }

    public void connect( String portName, Integer baudRate ) throws Exception
    {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if ( portIdentifier.isCurrentlyOwned() )
        {
            System.err.println("[ERROR] Port is currently in use");
        }
        else
        {
            commPort = portIdentifier.open(this.getClass().getName(), timeout);
            
            if ( commPort instanceof SerialPort )
            {
                serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(baudRate,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                
                InputStream in = serialPort.getInputStream();
                // OutputStream out = serialPort.getOutputStream();
                out = serialPort.getOutputStream();

                (new Thread(new SerialWriter(out))).start();
                
                serialPort.addEventListener(new SerialReader(in));
                serialPort.notifyOnDataAvailable(true);

            }
            else
            {
                System.err.println("[ERROR] Only serial ports are handled.");
            }
        }
        System.err.println(commPort.getName() + " connected.");
    }
    
    /**
     * Handles the input coming from the serial port. A new line character
     * is treated as the end of a block in this example. 
     */
    public static class SerialReader implements SerialPortEventListener 
    {
        private InputStream in;
        private byte[] buffer = new byte[1024];
        
        public SerialReader ( InputStream in )
        {
            this.in = in;
        }
        
        public void serialEvent(SerialPortEvent arg0) {
            int data;

            try
            {
                int len = 0;
                while ( ( data = in.read()) > -1 )
                {
                    if ( data == '\n' ) {
                        break;
                    }
                    buffer[len++] = (byte)data;
                }

                String uid = new String(buffer,0,len);
                
                // 要去叫dbmanager 
                Main.dbmanager.getEventDB().newEvent(uid);

            }
            catch ( IOException e )
            {
                e.printStackTrace();
                // System.exit(-1);
            }             
        }

    }

    /** */
    public static class SerialWriter implements Runnable 
    {
        OutputStream out;
        String inStr = null;

        public SerialWriter (OutputStream out)
        {
            this.out = out;
        }
        
        public void run()
        {
            // try
            // {
            //     // System.out.println("[SEND]" + inStr);
            //     // this.out.write(inStr.getBytes());
            //     // int c = 0;
            //     // while ( ( c = System.in.read()) > -1 )
            //     // {
            //     //     this.out.write(c);
            //     // }
            // }
            // catch ( IOException e )
            // {
            //     e.printStackTrace();
            //     // System.exit(-1);
            // }            
        }
    }

    public final boolean isConnected(){
		return (this.commPort!=null);
	}
    
    /**
     * Disconnect serial port
     */
    public void disconnect() {

		if (this.isConnected()) {
			this.commPort.close();
			System.out.println("Disconnected from Port " + commPort.getName());
			this.commPort = null;
		} else {
			System.out.println("There is nothing to disconnect");
		}
    }
    
    /**
     * Send byte array to serial port
     * @param output
     */
    public void send(final byte[] output) {
        try {
            out.write(output);
            out.flush();
        } catch (Exception e) {
            System.out.println("Port Not Avaialable (send) ");
        }
    }

    /**
     * Send string to serial port
     * @param outStr
     */
    public void send(final String outStr) {
        try {
            out.write(outStr.getBytes());
            out.flush();
        } catch (Exception e) {
            System.out.println("[ERROR] Port Not Avaialable (send) ");
        }
    }
}