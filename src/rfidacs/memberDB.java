package rfidacs;

import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.file.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import com.google.gson.*;

import main.utils;

/**
 * memberDB
 */
public class memberDB extends rfidDB {

    public static final String[] memberColumn = new String[] {"ID" ,"Name", "UID", "Permission", "Comment"};
    public static final String passImg = "./res/authImg.png";
    public static final String errorImg = "./res/errorImg.png";
    public static final String deniedImg = "./res/deniedImg.png";

    public memberDB(String path) {
        super(path);
    }

    /**
     * Create new member DB
     * @param path
     */
    public void createDB(final String path) {
        // TODO: handle file exist exception
        try {
            Class.forName("org.sqlite.JDBC");
            db = DriverManager.getConnection("jdbc:sqlite:" + path);
        
            Statement stmt = null;
            stmt = db.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS Member" +
                            "(ID TEXT PRIMARY KEY  NOT NULL," +
                            " NAME           TEXT  NOT NULL," + 
                            " UID            VARCHAR(8)  NOT NULL," + 
                            " Permission     INT   NOT NULL," + 
                            " Comment        TEXT)"; 
            stmt.executeUpdate(sql);
            stmt.close();
        }
        catch (final Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
    public void addMember() {

    }

    public void removeMember() {

    }

    /**
     * query member by UID
     * @param uid
     * @return
     */
    public boolean queryMember(final String uid, List<Object> ret) {

        String sql = "SELECT * FROM Member WHERE UID=X'" + uid + "'";

        try {  
            Statement stmt = db.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            if (!rs.next()) {
                System.out.println("Member not found.");
                return false;
            }
            else
            {
                ret.set(0, rs.getString("ID"));
                ret.set(1, rs.getString("Name"));
                ret.set(2, rs.getInt("Permission"));

                System.out.println(rs.getString("ID") +  "\t" + 
                                rs.getString("Name") + "\t" +
                                rs.getInt("Permission"));
            }

        }
        catch (SQLException e) {
            System.out.println(e.getMessage());  
        }
        
        return true;
    }

    public List<Object[]> queryAllMember() {
        String sql = "SELECT * FROM Member";
        List<Object[]> res = new ArrayList<Object[]>();

        try {
            Statement stmt  = db.createStatement();
            ResultSet rs    = stmt.executeQuery(sql); 

            // loop through the result set  
            while (rs.next()) {
                res.add(new Object[]{rs.getString("ID"), rs.getString("Name"),
                utils.encodeHexString(rs.getBytes("UID")), rs.getInt("Permission"), rs.getString("Comment")});
                
                System.out.println(rs.getString("ID") +  "\t" +
                                   rs.getString("Name") + "\t" +
                                   rs.getInt("Permission"));
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());  
        }
        return res;
    }
    
    /**
     *  Import members from JSON file
     * @param path
     * @return
     */
    public void importDB(final String path) {
        Gson gson = new Gson();
        Member[] members = null;

        String delSql = "DELETE * FROM Member";
        String insSql = "INSERT INTO Member VALUES(?, ?, ?, ?, ?)";
        
        // import member objects from json
        try {
            members = gson.fromJson(new FileReader(path), Member[].class);
        }    
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        // delete exist 
        try {
            Statement stmt  = db.createStatement();
            stmt.executeUpdate(delSql);
            stmt.close();
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());  
        }
        
        try {
            PreparedStatement stmt = db.prepareStatement(insSql);

            for (Member m : members) {
                stmt.setString(0, m.getID());
                stmt.setString(1, m.getName());
                stmt.setBytes(2, new BigInteger(m.getUID()).toByteArray());
                stmt.setInt(3, m.getPermission());
                stmt.setString(4, m.getComment());
                stmt.addBatch(delSql);
            }
            stmt.executeBatch();
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());  
        }
    }

    /**
     * Export members to JSON file
     * @param path
     */
    @Override
    public void exportDB(final String path) {
        
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
        String sql = "SELECT * FROM Member";
        
        try {
            Statement stmt  = db.createStatement();
            ResultSet rs    = stmt.executeQuery(sql); 
            Writer writer = Files.newBufferedWriter(Paths.get(path));

            List<Member> members = new ArrayList<Member>();
            while(rs.next()) {
                members.add(new Member(rs.getString("ID"), rs.getString("Name"),
                utils.encodeHexString(rs.getBytes("UID")), rs.getInt("Permission"), rs.getString("Comment")));                
            }
            gson.toJson(members, writer);
            writer.close();
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());  
        }
        catch (IOException ioe)
        {
            System.err.println(ioe.getMessage());
        }
    }

    
}