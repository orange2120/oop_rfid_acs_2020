package rfidacs;

import java.io.Serializable;

/**
 * Event class
 */
public class Event implements Serializable {
    private String datetime;
    private String uid;
    private int auth;

    public Event(String dt, String uid, int auth)
    {
        this.datetime = dt;
        this.uid = uid;
        this.auth = auth;
    }
}