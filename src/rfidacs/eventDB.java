package rfidacs;

import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.file.*;
import java.sql.*;
import java.text.*;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import com.google.gson.Gson;

import main.*;

/**
 * eventDB
 * database of event
 */
public class eventDB extends rfidDB {

    // column content for display in JTable
    public static final String[] eventColumn = new String[] {"Timestamp", "UID", "ID", "Name", "Authorized"};
    // columns in DB
    public static final String[] eventDBColumn = new String[] {"Timestamp", "UID", "Authorized"};

    public eventDB(String path) {
        super(path);
    }

    /**
     * Create a new event DB
     * @param path
     */
    public void createDB(final String path) {
        // Class.forName("org.sqlite.JDBC");
        Connection eventDB = null;
        try {
            eventDB = DriverManager.getConnection("jdbc:sqlite:" + path);
        
            Statement stmt = null;
            stmt = eventDB.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS Event" +
                            "(Timestamp DATETIME PRIMARY KEY NOT NULL" +
                            " UID              BLOB  NOT NULL," + 
                            " Authorized       INT   NOT NULL)"; 
            stmt.executeUpdate(sql);
            stmt.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Object[]> loadHistory() {
        
        List<Object[]> data = new ArrayList<Object[]>();
        String sql = "SELECT * FROM Event";

        
        
        try {  
            Statement stmt = db.createStatement();
            ResultSet rs   = stmt.executeQuery(sql);
            while (rs.next())
            {
                List<Object> retQuery = new ArrayList<Object>(Arrays.asList("-", "-", 0));
                String uid =  utils.encodeHexString(rs.getBytes("UID"));

                String id = "-";
                String name = "-";
                
                if(Main.dbmanager.getMemberDB().queryMember(uid, retQuery))
                {
                    // <ID>,<auth state>
                    id = (String)retQuery.get(0);
                    name = (String) retQuery.get(1);
                }
                data.add(new Object[]{rs.getString("Timestamp"), uid,
                id, name, rs.getInt("Authorized")});
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());  
        }
        return data;
    }

    /**
     * New event callback function
     * @param uid
     */
    public void newEvent(final String uid) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        System.out.println(dateFormat.format(date) + " " + uid);

        // TODO: 應該要用join去查DB，此法太爛
        List<Object> retQuery = new ArrayList<Object>(Arrays.asList("-", "-", 0));
        String outStr = ",";

        if(Main.dbmanager.getMemberDB().queryMember(uid, retQuery))
        {
            // <ID>,<auth state>
            outStr = retQuery.get(0) + "," + retQuery.get(2);
        }
        else
        {
            // Member not found
            outStr = "Auth failed!,";

        }
        Main_gui.com.send(outStr); // send query result to RFID reader
        
        // <datetime>,<uid>,<name>,<auth state>
        String datetime = dateFormat.format(date);
        String id = (String)retQuery.get(0);
        String name = (String) retQuery.get(1);
        int auth = (int)retQuery.get(2);
        String lastLogin = "";

        Object[] eventObj = {datetime, uid, id ,name, auth};
        Main.gui.addEventTable(eventObj);

        // query the last login event
        String queryLast = "SELECT Timestamp FROM Event WHERE UID=X'" + uid + "'";
        try {  
            Statement stmt = db.createStatement();
            ResultSet rs   = stmt.executeQuery(queryLast);
            while (rs.next())
            {
                lastLogin = rs.getString("Timestamp");
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());  
        }

        Main.gui.setInfo(id, name, lastLogin, auth); // update info panel
        writeEvent(datetime, uid, 1);
    }

    /**
     * Write an event to database
     * @param timestr
     * @param uid
     * @param auth
     */
    public void writeEvent(final String timestr, final String uid, final int auth) {
        String sql = "INSERT INTO Event(" + eventDBColumn[0] + ", " + eventDBColumn[1]
        + ", " + eventDBColumn[2] + ")\n"
        + "VALUES('" + timestr + "', X'" + uid + "', " + auth + ")";

        try {
            Statement stmt = db.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());  
        }
    }

    public void importDB(String path) {}

    public void exportDB(final String path)
    {
        Gson gson = new Gson();

        String sql = "SELECT * FROM Event";

        try {
            Statement stmt  = db.createStatement();
            ResultSet rs    = stmt.executeQuery(sql); 
            Writer writer = Files.newBufferedWriter(Paths.get(path));

            List<Event> events = new ArrayList<Event>();
            while(rs.next()) {
                events.add(new Event(rs.getString("Timestamp"), utils.encodeHexString(rs.getBytes("UID")), rs.getInt("Authorized")));                
            }
            gson.toJson(events, writer);
            writer.close();
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());  
        }
        catch (IOException ioe)
        {
            System.err.println(ioe.getMessage());
        }
    }

    public void queryByTime(final String timeStart, final String timeEnd) {
        // TODO: query events in an interval
    }
 
}