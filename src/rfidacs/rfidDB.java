package rfidacs;

import java.sql.*;

/**
 * rfidDB
 * Operation of DBs
 */
public abstract class rfidDB {
    
    protected Connection db = null;
    protected String dbPath = "";
    
    public rfidDB() {
        
    }

    public rfidDB(String path) {
        this.dbPath = path;
    }

    public void connectDB() {
        try {
            Class.forName("org.sqlite.JDBC");
            db = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
        }
        catch (final Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void disconnectDB() {
        try {
            db.close();
            db = null;
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public final boolean isDBConnected() {
        return db == null;
    }

    /**
     * Import DB content to JSON file
     * @param path
     */
    public abstract void importDB(final String path);

    /**
     *  Export DB content to JSON file
     *  @param path
     */
    public abstract void exportDB(final String path);

}