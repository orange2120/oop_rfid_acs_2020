package rfidacs;

import java.io.Serializable;

public class Member implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id = "";
    private String name = "";
    private String uid = "";
    private int permission = 0;
    private String commemt = "";

    public Member() {
        
    }
    
    public Member(String id, String n, String uid, int p) {
        this.name = n;
        this.id = id;
        this.uid = uid;
        this.permission = p;
    }

    public Member(String id, String n, String uid, int p, String comm) {
        this(id, n, uid, p);
        this.commemt = comm;
    }

    public final String getID() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String getUID() {
        return this.uid;
    }

    public final int getPermission() {
        return this.permission;
    }
    
    public final String getComment() {
        return this.commemt;
    }
}