/*
 * RFID card reader with 1602 LCD display
 * 
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 * I2C SDA
 * I2C
 */

#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#define BUZZER_PIN      4
#define BEEP_FREQ       880
#define AUTHORIZED_FREQ 587
#define DENIED_FREQ     698

#define RST_PIN         9
#define SS_PIN          10
MFRC522 mfrc522(SS_PIN, RST_PIN);

LiquidCrystal_I2C lcd(0x3F, 16,2);

char buf[64];
uint32_t lastEvent = 0;
uint32_t now = 0;

void setup()
{
    Serial.begin(115200);
    while (!Serial);

    SPI.begin();
    mfrc522.PCD_Init();
    delay(4);

    lcd.init();
    lcd.backlight();
    lcd.setCursor(0, 0);
    lcd.print("RFID card reader");
    delay(500);
    lcd.clear();
    lcd.noBacklight();
}

void loop()
{
    if (Serial.available()) 
    {
        String received = Serial.readStringUntil('\n');
        // data format : <ID>,<authorize state>
        received.toCharArray(buf, 50);
        char *p = buf;
        char *str;

        lcd.backlight();
        /*
        while ((str = strtok_r(p, ",", &p)) != NULL)
        {
            
            Serial.println(str);
        }
        */
        str = strtok_r(p, ",", &p);
        lcd.setCursor(0, 0);
        lcd.print(str);
        str = strtok_r(p, ",", &p);
        lcd.setCursor(0, 1);
        lcd.print(str);

        delay(2000);
        lcd.clear();
        lcd.noBacklight();
    }

    if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {
        byte *id = mfrc522.uid.uidByte;
        byte idSize = mfrc522.uid.size;
        char ch[2];
        for (byte i = 0; i < idSize; ++i)
        {
            sprintf(ch, "%02X", id[i]);
            Serial.print(ch);
        }
        Serial.write('\n');

        tone(BUZZER_PIN, BEEP_FREQ, 100);
        lcd.backlight();
    }
    mfrc522.PICC_HaltA();

}
