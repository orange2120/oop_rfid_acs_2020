package db;

import java.sql.*;

public class Db {
    public static void main(final String[] args) throws Exception {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");

            stmt = c.createStatement();
            String sql = "CREATE TABLE COMPANY " +
                            "(ID INT PRIMARY KEY     NOT NULL," +
                            " NAME           TEXT    NOT NULL, " + 
                            " AGE            INT     NOT NULL, " + 
                            " ADDRESS        CHAR(50), " + 
                            " SALARY         REAL)"; 
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();

        } catch (final Exception e) {
      
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
      
        }
    }
}