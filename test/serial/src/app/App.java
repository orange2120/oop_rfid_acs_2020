package app;

import gnu.io.*;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();    
        
        List<String> serialList = new ArrayList<>();

        while(ports.hasMoreElements())    
        {  
            CommPortIdentifier cpIdentifier = (CommPortIdentifier)ports.nextElement();  
            // System.out.println(cpIdentifier.getName());
            serialList.add(cpIdentifier.getName());
        }

        System.out.println("=====Available ports=====");

        for (int i = 0; i < serialList.size(); ++i) {
            System.out.println("[" + i + "] " + serialList.get(i));
        }
        System.out.println("=========================");

        Scanner scanner = new Scanner(System.in);
        int index = 0;
        try {
            System.out.println("");
            System.out.print("Input serial port index: ");
            index = scanner.nextInt();
            // System.out.println(scanner.next());
        }
        finally {
            scanner.close();
        }

        System.out.println("> " + serialList.get(index));

        TwoWaySerialComm com = new TwoWaySerialComm();

        int baud = 115200;
        try {
            // com.connect("/dev/ttyACM0", baud);
            com.connect(serialList.get(index), baud);
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }

        // com.disconnect();
    }
}